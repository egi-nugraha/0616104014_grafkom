﻿Public Class Form1
    Private Sub Form1_Load(sender As Object, e As EventArgs) Handles MyBase.Load
        PictureBox1.BackColor = Color.White
    End Sub

    Private Sub garis_submit_Click(sender As Object, e As EventArgs) Handles garis_submit.Click
        Dim g As Graphics = PictureBox1.CreateGraphics
        Dim img As New Bitmap(1, 1)
        img.SetPixel(0, 0, Color.Blue)

        Dim x1 = Convert.ToInt32(titik_awal_x.Text)
        Dim y1 = Convert.ToInt32(titik_awal_y.Text)
        Dim x2 = Convert.ToInt32(titik_akhir_x.Text)
        Dim y2 = Convert.ToInt32(titik_akhir_y.Text)

        Dim s
        Dim dx = x2 - x1
        Dim dy = y2 - y1

        Math.Abs(dx)
        Math.Abs(dy)

        If (Math.Abs(dx) > Math.Abs(dy)) Then
            s = Math.Abs(dx)
        Else
            s = Math.Abs(dy)
        End If

        Dim xi = dx / s
        Dim yi = dy / s

        Dim x = x1
        Dim y = y1

        g.DrawImage(img, x1, y1)

        Dim m = 0
        While m < s
            x += xi
            y += yi
            g.DrawImage(img, x, y)

            m = m + 1
        End While
    End Sub

    Private Sub lingkaran_submit_Click(sender As Object, e As EventArgs) Handles lingkaran_submit.Click
        Dim g As Graphics = PictureBox1.CreateGraphics
        Dim img As New Bitmap(1, 1)
        img.SetPixel(0, 0, Color.Red)

        Dim r = Convert.ToInt32(lingkaran_r.Text)
        Dim x = 0
        Dim y = r

        Dim p = 2 * (1 - r)
        Dim xc = Convert.ToInt32(lingkaran_x.Text)
        Dim yc = Convert.ToInt32(lingkaran_y.Text)

        While x < y
            x = x + 1
            If p < 0 Then
                p = p + 2 * x + 1
            Else
                y = y - 1
                p = p + 2 * (x - y) + 1
            End If

            g.DrawImage(img, xc + x, yc + y)
            g.DrawImage(img, xc + x, yc - y)
            g.DrawImage(img, xc - x, yc + y)
            g.DrawImage(img, xc - x, yc - y)

            g.DrawImage(img, xc + y, yc + x)
            g.DrawImage(img, xc + y, yc - x)
            g.DrawImage(img, xc - y, yc + x)
            g.DrawImage(img, xc - y, yc - x)
        End While
    End Sub

    Private Sub ellips_submit_Click(sender As Object, e As EventArgs) Handles ellips_submit.Click
        Dim g As Graphics = PictureBox1.CreateGraphics
        Dim img As New Bitmap(1, 1)
        img.SetPixel(0, 0, Color.Green)

        Dim xCentre = 0, yCentre = 0, p = 0, px = 0, py = 0, x = 0, y = 0

        g.DrawImage(img, xCentre + x, yCentre + y)
        g.DrawImage(img, xCentre - x, yCentre + y)
        g.DrawImage(img, xCentre + x, yCentre - y)
        g.DrawImage(img, xCentre - x, yCentre - y)

        Dim Rx = Convert.ToInt32(ellips_rx.Text)
        Dim Ry = Convert.ToInt32(ellips_ry.Text)

        Dim X1 = Convert.ToInt32(ellips_xc.Text)
        Dim Y1 = Convert.ToInt32(ellips_yc.Text)

        Dim Rx2 = Rx * Rx
        Dim Ry2 = Ry * Ry

        Dim twoRx2 = 2 * Rx2
        Dim twoRy2 = 2 * Ry2

        x = 0
        y = Ry
        p = Math.Round(Ry2 - Rx2 * Ry + (0.25 * Rx2))
        px = 0
        py = twoRx2 * y

        While px < py
            x = x + 1
            px = px + twoRy2
            If p >= 0 Then
                y = y - 1
                py = py - twoRx2
            End If

            g.DrawImage(img, x + X1, y + Y1)
            g.DrawImage(img, x * (-1) + X1, y + Y1)
            g.DrawImage(img, x * (-1) + X1, y * (-1) + Y1)
            g.DrawImage(img, x + X1, y * (-1) + Y1)

            If p < 0 Then
                p = p + Ry2 + px
            Else
                p = p + Ry2 + px - py
            End If
        End While

        p = Math.Round(Ry2 * (x + 0.5) * (x + 0.5) + Rx2 * (y - 1) * (y - 1) - Rx2 * Ry2)

        While y > 0
            y = y - 1
            py = py - twoRx2

            If p <= 0 Then
                x = x + 1
                px = px + twoRy2
            End If

            g.DrawImage(img, x + X1, y + Y1)
            g.DrawImage(img, x * (-1) + X1, y + Y1)
            g.DrawImage(img, x * (-1) + X1, y * (-1) + Y1)
            g.DrawImage(img, x + X1, y * (-1) + Y1)

            If p > 0 Then
                p = p + Rx2 - py
            Else
                p = p + Rx2 - py + px
            End If
        End While
    End Sub
End Class
