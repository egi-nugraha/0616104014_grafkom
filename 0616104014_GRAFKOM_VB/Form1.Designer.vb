﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class Form1
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.Label1 = New System.Windows.Forms.Label()
        Me.Label2 = New System.Windows.Forms.Label()
        Me.Label3 = New System.Windows.Forms.Label()
        Me.titik_awal_x = New System.Windows.Forms.TextBox()
        Me.titik_awal_y = New System.Windows.Forms.TextBox()
        Me.titik_akhir_x = New System.Windows.Forms.TextBox()
        Me.titik_akhir_y = New System.Windows.Forms.TextBox()
        Me.Label4 = New System.Windows.Forms.Label()
        Me.Label5 = New System.Windows.Forms.Label()
        Me.Label6 = New System.Windows.Forms.Label()
        Me.lingkaran_x = New System.Windows.Forms.TextBox()
        Me.lingkaran_y = New System.Windows.Forms.TextBox()
        Me.lingkaran_r = New System.Windows.Forms.TextBox()
        Me.PictureBox1 = New System.Windows.Forms.PictureBox()
        Me.lingkaran_submit = New System.Windows.Forms.Button()
        Me.garis_submit = New System.Windows.Forms.Button()
        Me.Label7 = New System.Windows.Forms.Label()
        Me.Label8 = New System.Windows.Forms.Label()
        Me.Label9 = New System.Windows.Forms.Label()
        Me.Label10 = New System.Windows.Forms.Label()
        Me.ellips_yc = New System.Windows.Forms.TextBox()
        Me.ellips_xc = New System.Windows.Forms.TextBox()
        Me.Label11 = New System.Windows.Forms.Label()
        Me.Label12 = New System.Windows.Forms.Label()
        Me.Label13 = New System.Windows.Forms.Label()
        Me.Label14 = New System.Windows.Forms.Label()
        Me.ellips_ry = New System.Windows.Forms.TextBox()
        Me.ellips_rx = New System.Windows.Forms.TextBox()
        Me.ellips_submit = New System.Windows.Forms.Button()
        Me.Label15 = New System.Windows.Forms.Label()
        Me.Label16 = New System.Windows.Forms.Label()
        Me.Label17 = New System.Windows.Forms.Label()
        CType(Me.PictureBox1, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'Label1
        '
        Me.Label1.AutoSize = True
        Me.Label1.Location = New System.Drawing.Point(31, 13)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(31, 13)
        Me.Label1.TabIndex = 0
        Me.Label1.Text = "Garis"
        '
        'Label2
        '
        Me.Label2.AutoSize = True
        Me.Label2.Location = New System.Drawing.Point(31, 38)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(53, 13)
        Me.Label2.TabIndex = 1
        Me.Label2.Text = "Titik Awal"
        '
        'Label3
        '
        Me.Label3.AutoSize = True
        Me.Label3.Location = New System.Drawing.Point(31, 64)
        Me.Label3.Name = "Label3"
        Me.Label3.Size = New System.Drawing.Size(54, 13)
        Me.Label3.TabIndex = 2
        Me.Label3.Text = "Titik Akhir"
        '
        'titik_awal_x
        '
        Me.titik_awal_x.Location = New System.Drawing.Point(90, 35)
        Me.titik_awal_x.Name = "titik_awal_x"
        Me.titik_awal_x.Size = New System.Drawing.Size(34, 20)
        Me.titik_awal_x.TabIndex = 3
        '
        'titik_awal_y
        '
        Me.titik_awal_y.Location = New System.Drawing.Point(130, 35)
        Me.titik_awal_y.Name = "titik_awal_y"
        Me.titik_awal_y.Size = New System.Drawing.Size(34, 20)
        Me.titik_awal_y.TabIndex = 4
        '
        'titik_akhir_x
        '
        Me.titik_akhir_x.Location = New System.Drawing.Point(90, 61)
        Me.titik_akhir_x.Name = "titik_akhir_x"
        Me.titik_akhir_x.Size = New System.Drawing.Size(34, 20)
        Me.titik_akhir_x.TabIndex = 5
        '
        'titik_akhir_y
        '
        Me.titik_akhir_y.Location = New System.Drawing.Point(130, 61)
        Me.titik_akhir_y.Name = "titik_akhir_y"
        Me.titik_akhir_y.Size = New System.Drawing.Size(34, 20)
        Me.titik_akhir_y.TabIndex = 6
        '
        'Label4
        '
        Me.Label4.AutoSize = True
        Me.Label4.Location = New System.Drawing.Point(31, 117)
        Me.Label4.Name = "Label4"
        Me.Label4.Size = New System.Drawing.Size(54, 13)
        Me.Label4.TabIndex = 7
        Me.Label4.Text = "Lingkaran"
        '
        'Label5
        '
        Me.Label5.AutoSize = True
        Me.Label5.Location = New System.Drawing.Point(31, 143)
        Me.Label5.Name = "Label5"
        Me.Label5.Size = New System.Drawing.Size(57, 13)
        Me.Label5.TabIndex = 8
        Me.Label5.Text = "Titik Pusat"
        '
        'Label6
        '
        Me.Label6.AutoSize = True
        Me.Label6.Location = New System.Drawing.Point(31, 169)
        Me.Label6.Name = "Label6"
        Me.Label6.Size = New System.Drawing.Size(39, 13)
        Me.Label6.TabIndex = 9
        Me.Label6.Text = "Jari-jari"
        '
        'lingkaran_x
        '
        Me.lingkaran_x.Location = New System.Drawing.Point(90, 140)
        Me.lingkaran_x.Name = "lingkaran_x"
        Me.lingkaran_x.Size = New System.Drawing.Size(34, 20)
        Me.lingkaran_x.TabIndex = 10
        '
        'lingkaran_y
        '
        Me.lingkaran_y.Location = New System.Drawing.Point(130, 140)
        Me.lingkaran_y.Name = "lingkaran_y"
        Me.lingkaran_y.Size = New System.Drawing.Size(34, 20)
        Me.lingkaran_y.TabIndex = 11
        '
        'lingkaran_r
        '
        Me.lingkaran_r.Location = New System.Drawing.Point(90, 166)
        Me.lingkaran_r.Name = "lingkaran_r"
        Me.lingkaran_r.Size = New System.Drawing.Size(34, 20)
        Me.lingkaran_r.TabIndex = 12
        '
        'PictureBox1
        '
        Me.PictureBox1.Location = New System.Drawing.Point(213, 13)
        Me.PictureBox1.Name = "PictureBox1"
        Me.PictureBox1.Size = New System.Drawing.Size(511, 414)
        Me.PictureBox1.TabIndex = 13
        Me.PictureBox1.TabStop = False
        '
        'lingkaran_submit
        '
        Me.lingkaran_submit.Location = New System.Drawing.Point(34, 196)
        Me.lingkaran_submit.Name = "lingkaran_submit"
        Me.lingkaran_submit.Size = New System.Drawing.Size(130, 23)
        Me.lingkaran_submit.TabIndex = 14
        Me.lingkaran_submit.Text = "Buat Lingkaran"
        Me.lingkaran_submit.UseVisualStyleBackColor = True
        '
        'garis_submit
        '
        Me.garis_submit.Location = New System.Drawing.Point(34, 87)
        Me.garis_submit.Name = "garis_submit"
        Me.garis_submit.Size = New System.Drawing.Size(130, 23)
        Me.garis_submit.TabIndex = 15
        Me.garis_submit.Text = "Buat Garis"
        Me.garis_submit.UseVisualStyleBackColor = True
        '
        'Label7
        '
        Me.Label7.AutoSize = True
        Me.Label7.Location = New System.Drawing.Point(101, 124)
        Me.Label7.Name = "Label7"
        Me.Label7.Size = New System.Drawing.Size(14, 13)
        Me.Label7.TabIndex = 16
        Me.Label7.Text = "X"
        '
        'Label8
        '
        Me.Label8.AutoSize = True
        Me.Label8.Location = New System.Drawing.Point(141, 124)
        Me.Label8.Name = "Label8"
        Me.Label8.Size = New System.Drawing.Size(14, 13)
        Me.Label8.TabIndex = 17
        Me.Label8.Text = "Y"
        '
        'Label9
        '
        Me.Label9.AutoSize = True
        Me.Label9.Location = New System.Drawing.Point(141, 238)
        Me.Label9.Name = "Label9"
        Me.Label9.Size = New System.Drawing.Size(20, 13)
        Me.Label9.TabIndex = 23
        Me.Label9.Text = "Yc"
        '
        'Label10
        '
        Me.Label10.AutoSize = True
        Me.Label10.Location = New System.Drawing.Point(101, 238)
        Me.Label10.Name = "Label10"
        Me.Label10.Size = New System.Drawing.Size(20, 13)
        Me.Label10.TabIndex = 22
        Me.Label10.Text = "Xc"
        '
        'ellips_yc
        '
        Me.ellips_yc.Location = New System.Drawing.Point(133, 254)
        Me.ellips_yc.Name = "ellips_yc"
        Me.ellips_yc.Size = New System.Drawing.Size(34, 20)
        Me.ellips_yc.TabIndex = 21
        '
        'ellips_xc
        '
        Me.ellips_xc.Location = New System.Drawing.Point(93, 254)
        Me.ellips_xc.Name = "ellips_xc"
        Me.ellips_xc.Size = New System.Drawing.Size(34, 20)
        Me.ellips_xc.TabIndex = 20
        '
        'Label11
        '
        Me.Label11.AutoSize = True
        Me.Label11.Location = New System.Drawing.Point(34, 257)
        Me.Label11.Name = "Label11"
        Me.Label11.Size = New System.Drawing.Size(57, 13)
        Me.Label11.TabIndex = 19
        Me.Label11.Text = "Titik Pusat"
        '
        'Label12
        '
        Me.Label12.AutoSize = True
        Me.Label12.Location = New System.Drawing.Point(34, 231)
        Me.Label12.Name = "Label12"
        Me.Label12.Size = New System.Drawing.Size(31, 13)
        Me.Label12.TabIndex = 18
        Me.Label12.Text = "Ellips"
        '
        'Label13
        '
        Me.Label13.AutoSize = True
        Me.Label13.Location = New System.Drawing.Point(141, 281)
        Me.Label13.Name = "Label13"
        Me.Label13.Size = New System.Drawing.Size(20, 13)
        Me.Label13.TabIndex = 27
        Me.Label13.Text = "Ry"
        '
        'Label14
        '
        Me.Label14.AutoSize = True
        Me.Label14.Location = New System.Drawing.Point(101, 281)
        Me.Label14.Name = "Label14"
        Me.Label14.Size = New System.Drawing.Size(20, 13)
        Me.Label14.TabIndex = 26
        Me.Label14.Text = "Rx"
        '
        'ellips_ry
        '
        Me.ellips_ry.Location = New System.Drawing.Point(133, 297)
        Me.ellips_ry.Name = "ellips_ry"
        Me.ellips_ry.Size = New System.Drawing.Size(34, 20)
        Me.ellips_ry.TabIndex = 25
        '
        'ellips_rx
        '
        Me.ellips_rx.Location = New System.Drawing.Point(93, 297)
        Me.ellips_rx.Name = "ellips_rx"
        Me.ellips_rx.Size = New System.Drawing.Size(34, 20)
        Me.ellips_rx.TabIndex = 24
        '
        'ellips_submit
        '
        Me.ellips_submit.Location = New System.Drawing.Point(34, 323)
        Me.ellips_submit.Name = "ellips_submit"
        Me.ellips_submit.Size = New System.Drawing.Size(130, 23)
        Me.ellips_submit.TabIndex = 28
        Me.ellips_submit.Text = "Buat Ellips"
        Me.ellips_submit.UseVisualStyleBackColor = True
        '
        'Label15
        '
        Me.Label15.AutoSize = True
        Me.Label15.Location = New System.Drawing.Point(20, 366)
        Me.Label15.Name = "Label15"
        Me.Label15.Size = New System.Drawing.Size(114, 13)
        Me.Label15.TabIndex = 29
        Me.Label15.Text = "Hak Cipta 20 Juli 2019"
        '
        'Label16
        '
        Me.Label16.AutoSize = True
        Me.Label16.Location = New System.Drawing.Point(20, 385)
        Me.Label16.Name = "Label16"
        Me.Label16.Size = New System.Drawing.Size(151, 13)
        Me.Label16.TabIndex = 30
        Me.Label16.Text = "0616104014 - EGI NUGRAHA"
        '
        'Label17
        '
        Me.Label17.AutoSize = True
        Me.Label17.Location = New System.Drawing.Point(20, 406)
        Me.Label17.Name = "Label17"
        Me.Label17.Size = New System.Drawing.Size(177, 13)
        Me.Label17.TabIndex = 31
        Me.Label17.Text = "0616104002 - AZHAR DZULFIQAR"
        '
        'Form1
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(762, 450)
        Me.Controls.Add(Me.Label17)
        Me.Controls.Add(Me.Label16)
        Me.Controls.Add(Me.Label15)
        Me.Controls.Add(Me.ellips_submit)
        Me.Controls.Add(Me.Label13)
        Me.Controls.Add(Me.Label14)
        Me.Controls.Add(Me.ellips_ry)
        Me.Controls.Add(Me.ellips_rx)
        Me.Controls.Add(Me.Label9)
        Me.Controls.Add(Me.Label10)
        Me.Controls.Add(Me.ellips_yc)
        Me.Controls.Add(Me.ellips_xc)
        Me.Controls.Add(Me.Label11)
        Me.Controls.Add(Me.Label12)
        Me.Controls.Add(Me.Label8)
        Me.Controls.Add(Me.Label7)
        Me.Controls.Add(Me.garis_submit)
        Me.Controls.Add(Me.lingkaran_submit)
        Me.Controls.Add(Me.PictureBox1)
        Me.Controls.Add(Me.lingkaran_r)
        Me.Controls.Add(Me.lingkaran_y)
        Me.Controls.Add(Me.lingkaran_x)
        Me.Controls.Add(Me.Label6)
        Me.Controls.Add(Me.Label5)
        Me.Controls.Add(Me.Label4)
        Me.Controls.Add(Me.titik_akhir_y)
        Me.Controls.Add(Me.titik_akhir_x)
        Me.Controls.Add(Me.titik_awal_y)
        Me.Controls.Add(Me.titik_awal_x)
        Me.Controls.Add(Me.Label3)
        Me.Controls.Add(Me.Label2)
        Me.Controls.Add(Me.Label1)
        Me.Name = "Form1"
        Me.Text = "0616104014 GRAFKOM"
        CType(Me.PictureBox1, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub

    Friend WithEvents Label1 As Label
    Friend WithEvents Label2 As Label
    Friend WithEvents Label3 As Label
    Friend WithEvents titik_awal_x As TextBox
    Friend WithEvents titik_awal_y As TextBox
    Friend WithEvents titik_akhir_x As TextBox
    Friend WithEvents titik_akhir_y As TextBox
    Friend WithEvents Label4 As Label
    Friend WithEvents Label5 As Label
    Friend WithEvents Label6 As Label
    Friend WithEvents lingkaran_x As TextBox
    Friend WithEvents lingkaran_y As TextBox
    Friend WithEvents lingkaran_r As TextBox
    Friend WithEvents PictureBox1 As PictureBox
    Friend WithEvents lingkaran_submit As Button
    Friend WithEvents garis_submit As Button
    Friend WithEvents Label7 As Label
    Friend WithEvents Label8 As Label
    Friend WithEvents Label9 As Label
    Friend WithEvents Label10 As Label
    Friend WithEvents ellips_yc As TextBox
    Friend WithEvents ellips_xc As TextBox
    Friend WithEvents Label11 As Label
    Friend WithEvents Label12 As Label
    Friend WithEvents Label13 As Label
    Friend WithEvents Label14 As Label
    Friend WithEvents ellips_ry As TextBox
    Friend WithEvents ellips_rx As TextBox
    Friend WithEvents ellips_submit As Button
    Friend WithEvents Label15 As Label
    Friend WithEvents Label16 As Label
    Friend WithEvents Label17 As Label
End Class
